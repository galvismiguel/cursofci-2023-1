import numpy as np
import sympy as sp
from distribucion import normal

if __name__ == "__main__":

    n = 100000 # número de iteraciones
    h = 0.4    # máximo para la función gaussiana
    s = 1      # desviación estándar
    u = 0      # media

    dist = normal(n, h, s, u)
    print(dist.distribucion())