from Clase_Integracion import *
if __name__ == "__main__":
    # Numero de iteraciones
    N = 10000
    
    # Declarando la clase
    integracion = Integral()
    
    # Montecarlo
    print("Obtenemos por Montecarlo:", integracion.solve_montecarlo(N))

    # Analitico
    print("Obtenido analiticamente", integracion.solve_exact())
    
    # Comparacion
    integracion.compare_sols(N)