import numpy as np
from Metropolis import *
class HarmOscMontecarlo():
    """
    La clase contiene un metodo que calcula la energia promedio basada dado un parametro variacional
    alpha. No requiere ningun parametro de inicializacion ya que el parametro se toma como entrada
    del metodo.
    Nota: La integracion de metropolis implementada continua siendo muy sensible a cambios en los 
    hiperparametros. Hice pruebas hasta que los resultados de las integrales en regiones cercanas a 
    la teorica fueran mas o menos consistentes con lo esperado. Si se toma alpha grande es posible 
    que la integrales no converjan correctamente.  
    """
    def ground_state_metropolis(self, alpha):
        f = lambda x: (alpha + (x**2)*(1/2-2*(alpha**2)))
        w = lambda x: np.sqrt(2*alpha/np.pi) * np.exp(-2*alpha*(x**2))
        Nwalkers = 200
        Nsteps = 30000
        x0start = -10
        xfstart = 10
        tempint = Metropolis(f, w, Nwalkers, Nsteps, x0start, xfstart)
        return tempint.Integrate()