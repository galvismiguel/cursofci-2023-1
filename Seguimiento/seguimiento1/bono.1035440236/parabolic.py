import math
import numpy as np
import matplotlib.pyplot as plt

class tiroParabolico():

    def __init__(self, velinit, alpha, g, h0,x0,):
        print('Inicializando clase tiroParabolico')

        #Parámetros.

        self.velinit = velinit  # velocidad inicial
        self.radinalpha = math.radians(alpha) # angulo en radianes
        self.g = g # gravedad
        self.h0 = h0 # altua incial
        self.x0= x0 # x inicial


    def velX(self): # fedinicion de 
        vel_x = self.velinit*round(math.cos(self.radinalpha),3)
        return vel_x

    def velY(self):
        vel_y = self.velinit*round(math.sin(self.radinalpha),3)
        return vel_y

    def tMaxVuelo(self): # Se va a usar luego para herencias y bonificación.
        try:
            #tmax = -2*self.velY()/self.g
            tmax = (-self.velY() - np.sqrt(self.velY()**2 - 2*self.g*self.h0))/self.g
            return tmax
        except: 
            return 'Error en cálculo de tmax, revisar parámetros.'
    def arrTime(self):
        aar_time=np.arange(0,self.tMaxVuelo(),0.001)   
        return aar_time
    def posX(self):
        posx=[self.x0+i*self.velX() for i in self.arrTime()]
        return posx
    def posY(self):
        posy=[self.h0+i*self.velY()+ (1/2)*self.g*i**2 for i in self.arrTime()]
        return posy
    def figMp(self):
        plt.figure(figsize=(10,8))
        plt.plot(self.posX(), self.posY())
        plt.savefig("parabolic.png")
class parabolico2(tiroParabolico):
    def __init__(self, velinit, alpha, g, h0, x0,aceleracion):
        super().__init__(velinit, alpha, g, h0, x0)
        self.aceleracion=aceleracion
    def posY1(self):
        posy=[self.h0+i*self.velY()+ (1/2)*self.g*i**2 for i in self.arrTime()]
        return posy
    def posX1(self):
        posx=[self.x0+i*self.velX()+ (1/2)*self.aceleracion*i**2 for i in self.arrTime()]
        return posx
    def grafica(self):    
        plt.figure(figsize=(10,8))
        plt.plot(self.posX1(), self.posY1())
        plt.savefig("parabolic1.png")


        #posx=[self.x0+i*self.velX() for i in self.arrTime()]
        


# herencia    

    
     

 