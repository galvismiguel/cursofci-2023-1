from EDO import EDO
import sympy as sym

if __name__=="__main__":
    a,b,n,y0,limit=0,1,100,-1,0.8

    f=lambda x,y: sym.exp(-x)

    sol=EDO(a,b,n,y0,f,limit)
    print("Metodo de euler: {}".format(sol.euler()[1][-1]))
    print("Metodo analitico: {}".format(sol.metodo_analitico()[1][-1]))
    graf=sol.figEDO()
