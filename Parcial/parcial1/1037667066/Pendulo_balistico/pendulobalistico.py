import numpy as np
import math 
import matplotlib.pyplot as plt


class Pendulo_balistico():
    def __init__(self,mb,Mp,g,R,angulo):
        #Se considera el bloque como una masa puntual, suspendida de una cuerda sin masa e inextensible

        #Constructor
        self.a=math.radians(angulo)  #Angulo de desvaición [°](debe estar entre 0° y 180°)
        self.mb=mb #Masa de la bala  [Kg]
        self.Mp=Mp #Masa del bloque en el pendulo [Kg]
        self.R=R   #Longitud del pendulo [m]
        self.g=g   #Gravedad [m/s^2]
        self.w0=np.sqrt(self.g/self.R) #Frecuencia natural del pendulo [rad/s]

    def velocidadbala(self): #Metodo para la velocidad inicial de la bala
        return (self.Mp+self.mb)/self.mb*(np.sqrt(2*self.g*self.R*(1-np.cos(self.a))))

    def desviacionangulo(self): #metodo que retorne la desviación del angulo
        return self.a
            
    def graf(self):   #Grafica de la oscilación del pendulo despues del choque
        f= lambda t: np.degrees(self.a)*np.cos(self.w0*t) 
        t=np.arange(0,15,0.01)
        plt.plot(t,f(t))
        plt.title('Oscilación del pendulo balistico',fontsize=15)
        plt.ylabel(r'$\theta (t)$ (°)',fontsize=15)
        plt.xlabel('t (s)',fontsize=15)
        plt.savefig('Oscilación del pendulo balistico')
        
class pendulo_balistico2(Pendulo_balistico):

    ''' Herencia y polimorfismo: Se determina la velocidad minima de la bala para que el pendulo de un giro completo,
     y se aplica el concepto de polimorfismo para determinar el angulo maximo de desviación dada una velocidad inicial
     de la bala'''

    def __init__(self,mb,Mp,g,R, v0, angulo = False):
        super().__init__(mb,Mp,g,R,angulo)
        self.v0=v0
        self.a=self.desviacionangulo()
    
    def velocidadminbala(self): #Metodo para determinar la velocidad minima de la bala para que el pendulo de un giro completo
        if self.mb ==0 or self.Mp==0:
            return "No hay movimiento, la masas no deben ser cero"
        else:
            return  ((self.mb+self.Mp)/self.mb)*np.sqrt(5*self.R*self.g)
        
    def desviacionangulo(self): #metodo para determinar el angulo de desviación maxima
        a=(1/(2*self.g*self.R))*(self.mb*self.v0/(self.Mp+self.mb))**2
        if a <= 2: #valores para los cuales en coseno inverso no esta definido
            return np.rad2deg(np.arccos(1-a))
        else:
            return 'Cambiar los argumentos de entrada'
    