import numpy as np
from MetodosNum import MetodosNum

'''
Execution del problema 3.1, solución de ecuaciones diferenciales, comparando la 
solución analítica y la solución númerica implementada con el método de Euler
'''
# definimos la ecuación diferencial a resolver
def f(x, y):
    return np.exp(-x)


if __name__ == "__main__":
    # condiciones inciales
    a = 0  # límite inferior
    b = 1  # límite superior
    n = 100 # pasos
    y0 = -1 # condición inicial

    # llamamos la clase de métodos numéricos
    sol1 = MetodosNum(a, b, y0, n, f, point=False)
    print(sol1.graficar())
