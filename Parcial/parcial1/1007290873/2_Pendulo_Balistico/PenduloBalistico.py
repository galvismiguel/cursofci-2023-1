import numpy as np 
import matplotlib.pyplot as plt

class penduloBalistico():

    """
    Clase que permite describir el problema del péndulo balístico. Permite determinar la velocidad 
    de la bala dado el ángulo alpha que se desvía el péndulo después que la bala colisiona con el bloque

    """

    # Atributos de clase
    
    # Aceleración gravitacional
    g = 9.81 # m/s^2

    # Velocidad de la luz
    c = 3e8 # m/s

    # Constructor

    def __init__(self, L, M, m, alpha):

        """
        Parámetros con los que se instancia la clase
                                                                                                      |  Unidades
        L: longitud de la cuerda de la que cuelga el bloque                                           |   [m]
        M: masa del bloque sobre el que colisiona la bala                                             |   [kg]
        m: masa de la bala                                                                            |   [kg]
        alpha: ángulo que alcanza el bloque tras la colisión con la bala (este debe ser menor a 180°) |   [grados]
        """

        # Definición de atributos de instancia

        self.longitud = L
        self.masa_bloque = M
        self.masa_bala = m
        self.angulo_desviacion = alpha

    # Definición de métodos

    def DesviacionAngulo(self):
        
        # Método que retorno el ángulo que alcanza el bloque tras la colisión con la bala, en grados. 

        return self.angulo_desviacion

    def VelocidadBala(self):

        # Método que retorna velocidad inicial de la bala, en m/s

        square_root_expr = np.sqrt(2 * self.g * self.longitud * (1 - np.cos(np.deg2rad(self.DesviacionAngulo()))))
        return square_root_expr *  (1 + (self.masa_bloque / self.masa_bala))


    def GraficaOscilacion(self):
        
        # Gráfica de la oscilación del péndulo, asumimos que el tiempo inicial coincide con
        # el instante en que el péndulo alcanza el ángulo de desviación

        # Se crea un array de tiempo
        tiempos = np.linspace(0, 10, 500)

        frecuencia_angular = np.sqrt(self.g / self.longitud)

        arreglo_angulo = self.DesviacionAngulo() * np.cos(frecuencia_angular * tiempos)

        fig, ax = plt.subplots(figsize=(18,15))
        ax.plot(tiempos, arreglo_angulo, color='red', label = "Oscilación")
        ax.set_title("Gráfica de oscilación del péndulo (bloque unido con la bala)", fontsize=20)
        ax.set_xlabel("$t$ (s)",fontsize=20)
        ax.set_ylabel(r"$\theta(t)$ (°)",fontsize=20)
        ax.legend(fontsize = 16) 
        ax.grid()
        fig.savefig("OscilacionPenduloBalistico.png")


class penduloBalisticoGeneral(penduloBalistico):

    """
    Clase que permite describir el problema del péndulo balístico. Esta generaliza la clase penduloBalistico
    en el sentido en que además de poder determinar la velocidad de la bala dado el ángulo de desviación,
    también permite ingresar un valor de velocidad inicial para determinar el ángulo de desviación correspondiente
    del cuerpo bloque más bala. 
    Esta clase parmite también calcular la velocidad mínima que se requiere para dar una vuelta completa del péndulo resultante,
    a partir de los parámetro longitud de la cuerda, masa del bloque y masa de la bala.

    """

    # Constructor

    def __init__(self, L, M, m, alpha = False, v0 = False):

        """
        Parámetros con los que se instancia la clase
                                                                                                        |  Unidades
        L: longitud de la cuerda de la que cuelga el bloque                                             |   [m]
        M: masa del bloque sobre el que colisiona la bala                                               |   [kg]
        m: masa de la bala                                                                              |   [kg]
        v0: velocidad inicial de la bala                                                                |   [m/s]
        alpha: ángulo que alcanza el bloque tras la colisión con la bala (este debe ser menor a 180º)   |   [grados]

        Estos dos últimos atributos toman por default el valor False
        Para poder usar los métodos y que la clase tenga una utilidad se debe ingresar
        un valor para alguno de estos dos atributos, es decir cambiar el valor predeterminado
        de False ya sea para la velocidad inicial o para el ángulo 
        """

        # Definición de atributos

        # Se heredan los atributos de la clase 
        super().__init__(L, M, m, alpha)

        self.velocidad_inicial = v0 

    # Definición de métodos modificados o nuevos con respecto a tiroParabolico

    def VelocidadMinimaUnaVuelta(self):
        return np.sqrt(5 * self.longitud * self.g) * (1 + (self.masa_bloque / self.masa_bala))

    def DesviacionAngulo(self):
        
        # Método que retorno el ángulo que alcanza el bloque tras la colisión con la bala, en grados. 

        # Si se ingreso este valor al instanciar la clase, se retorna este mismo valor
        if self.angulo_desviacion:
            return self.angulo_desviacion

        # Sino se retorna el valor calculado a partir de la velocidad inicial de la bala
        else:
            # Expresión que se evalua en la función coseno inverso
            denominador_expr = 2 * self.g * self.longitud * (self.masa_bala + self.masa_bloque)**2
            inverse_cos_argument = 1 - (( self.masa_bala * self.velocidad_inicial )** 2) / (denominador_expr)

            # La velocidad debe ser menor que la de la luz
            if self.velocidad_inicial >= self.c:
                raise ValueError("La velocidad debe ser menor que c")
            # El argumento de la función coseno inverso debe pertenecer al dominio donde está definida
            elif inverse_cos_argument < -1:
                raise ValueError("Revisar la elección de valores realistas para el problema")
            # Cálculo del ángulo
            else:
                calcular_alpha = np.arccos(inverse_cos_argument)
                return np.rad2deg(calcular_alpha)

    def VelocidadBala(self):

        # Método que retorna velocidad inicial de la bala, en m/s

        # Si se ingreso este valor al instanciar la clase, se retorna este mismo valor
        if self.velocidad_inicial:
            return self.velocidad_inicial

        # Sino se retorna el valor calculado a partir del ángulo que alcanza el bloque tras la colisión con la bala
        else:
            square_root_expr = np.sqrt(2 * self.g * self.longitud * (1 - np.cos(np.deg2rad(self.DesviacionAngulo()))))
            return square_root_expr *  (1 + (self.masa_bloque / self.masa_bala))

    # Definimos un nuevo método de gráfica que incluye una gráfica de energía, pero se hereda también el método GraficaOscilacion
    # de la clase penduloBalistico 
    def GraficaOscilacionConEnergía(self):
        
        # Gráfica de la oscilación del péndulo, asumimos que el tiempo inicial coincide con
        # el instante en que el péndulo alcanza el ángulo de desviación
        # Se grafica también la energía cinética y la potencial

        tiempos = np.linspace(0, 5, 800)

        frecuencia_angular = np.sqrt(self.g / self.longitud)

        arreglo_angulo = np.deg2rad(self.DesviacionAngulo()) * np.cos(frecuencia_angular * tiempos)

        arreglo_vel_angular = np.deg2rad(self.DesviacionAngulo()) * frecuencia_angular * np.sin(frecuencia_angular * tiempos)
        
        energia_cinetica = 0.5 * (self.masa_bala + self.masa_bloque) * (self.longitud **2) * (arreglo_vel_angular**2) 

        energia_potencial = (self.masa_bala + self.masa_bloque) * self.g * self.longitud * (1 - np.cos(arreglo_angulo))

        energia_total = energia_cinetica + energia_potencial

        fig, ax = plt.subplots(1, 2, figsize=(32,17))
        fig.tight_layout(pad=3)

        ax[0].plot(tiempos, np.rad2deg(arreglo_angulo), color='green', label = "Oscilación")
        ax[0].set_title("Gráfica de oscilación del péndulo (bloque unido con la bala)", fontsize=20)
        ax[0].set_xlabel("$t$ (s)",fontsize=20)
        ax[0].set_ylabel(r"$\theta(t)$ (°)",fontsize=20)
        ax[0].legend(fontsize = 16) 
        ax[0].grid()

        ax[1].plot(tiempos, energia_cinetica, color='red', label = "Energía Cinética")
        ax[1].plot(tiempos, energia_potencial, color='blue', label = "Energía Potencial")
        ax[1].plot(tiempos, energia_total, color = 'orange', label ="Energía Total")
        ax[1].set_title("Gráfica de la evolución de la energía del pendulo (bloque unido con la bala)", fontsize=20)
        ax[1].set_xlabel("$t$ (s)",fontsize=20)
        ax[1].set_ylabel("Energía (J)",fontsize=20)
        ax[1].legend(fontsize = 16) 
        ax[1].grid()

        fig.savefig("OscilacionPenduloBalisticoConEnergia.png")
