import numpy as np
from PenduloSimple import *


class PenduloBalistico():

    # Constructor de la clase
    def __init__(self, m_bala,m_bloque,Longitud,v_bala= 0.,Angle = 0.,g = 9.8):
        
        print('Inicializando PenduloBalistico')

        # Verificando condiciones iniciales:
        if (m_bala == 0) or (m_bloque == 0) or (Longitud == 0) or (g == 0) or (v_bala > 2.99e8):
            print('Los valores ingresados no darán una solución apropiada')
            raise ValueError
        else:
            # Si no hay error
            self.m_bala = m_bala
            self.m_bloque = m_bloque
            self.L = Longitud
            self.g = g
        
        # Inicializando variable a usar
        self.V_pendulo = 0

        # Verificando incognitas del problema
        if not(bool(v_bala) ^ bool(Angle)): # Se usa la NOTXOR para verificar que solo se está ingresando una de las dos incognitas
            print('Está inicializando un objeto sin condiciones iniciales suficientes o con demasiadas')
            raise ValueError
        else: 
            self.Angle = np.deg2rad(Angle)
            self.v0 = v_bala 
        

    def VelocidadPendulo(self):
        # Función para calcular la velocidad que adquiere el péndulo 
        # cuando la bala impacta en él.

        if bool(self.v0): # En el caso en que conocemos la velocidad inicial
            return (self.m_bala*self.v0)/(self.m_bala + self.m_bloque)
        else: # En el caso que conocemos el ángulo de desplazamiento
            return np.sqrt(2*self.L*self.g*(1-np.cos(self.Angle)))
    
    def VelocidadMinima(self):
        # Función para calcular la velocidad mínima necesaria para que el péndulo
        # logre dar una vuelta completa.
        return 2*np.sqrt(self.L*self.g)*(self.m_bala+self.m_bloque)/self.m_bala   

    def AnguloElevacion(self):
        # Función para calcular el ángulo de elevación que tiene el péndulo
        # con una velocidad inicial conocida

        if self.VelocidadMinima() > float(self.v0):
            # La velocidad inicial debe ser menor que la velocidad mínima
            # para que el péndulo complete una vuelta

            cos_th = 1 - (self.VelocidadPendulo()**2)/(2*self.L*self.g) # Calculando el valor de cos(\theta)
            self.Angle = round(np.arccos(cos_th),3)
            return self.Angle
        else:
            # Si la condición no se cumpla, se informa que:
            print('La velocidad hace que el bloque de vueltas indefinidamente')
            print('Puede probar aumentando la masa del bloque')
            return self.Angle # Se devuelve el valor por defecto
        
    
    def Grafica(self,TFinal= 10,name='Oscilador_Balístico',method = 'RK4',N = 500):

        '''Grafíca del péndulo balístico, es decir, luego de la colisión se tiene que 
           el sistema bloque-bala forman un pendulo el cuál comienza a oscilar.
            En esta aproximación se utiliza la ecuación de un oscilador armónico. 
            solucionada usando la clase PenduloSimple, la cual soluciona la ecuación:
            theta'' = -omega²*theta
            Por defecto se soluciona utilizando el método numérico Runge-Kutta 3 por su precisión '''
        
        # Se toma como condición inicial el momento en el que la bala colisiona contra el bloque
        CondIni = [0,self.VelocidadPendulo()] 

        Omega = np.sqrt(self.g/self.L) # Calculando Omega

        Osc_Bal = PenduloSimple(CondIni,Omega,TFinal) # Se inicializa el objeto 

        Osc_Bal.Grafica(name=name,method = method, N= N) # Se utiliza el método propio de la clase para graficar
        

    

    


    