## **Reducción de Orden de la Ecuación Diferencial del Péndulo Simple**
---

En general sabemos que la ecuación del Péndulo Simple es de la forma:
$$\frac{d²}{dt²}\theta(t) = -\frac{g}{l}\sin{\theta}$$
Para pequeños cambios de $\theta$
$$\frac{d²}{dt²}\theta(t) \approx -\omega² \theta(t)$$

Con $\displaystyle\omega² = \frac{g}{l}$.

Sea $y(t)$ una función continua, tal que

$$\frac{d}{dt}\theta(t) = y(t)$$
Entonces 
$$\frac{d}{dt}y(t) =-\omega² \theta(t)$$

Por tanto el problema del péndulo simple se reduce a la solución de los ecuaciones diferenciales de primer orden acopladas.

El programa creado está diseñado para resolver las ecuaciones a partir de la creación de un vector de la forma:

$$ S(t) = \begin{bmatrix}
    \theta(t)\\ \dot{\theta}(t)
\end{bmatrix} = \begin{bmatrix}
    \theta(t)\\ y(t)
\end{bmatrix}$$

tal que 
$$ \dot{S}(t) = \begin{bmatrix}
    \dot{\theta}(t)\\ \ddot{\theta}(t)
\end{bmatrix} = \begin{bmatrix}
    y(t)\\ -\omega² \,\theta(t)
\end{bmatrix}$$