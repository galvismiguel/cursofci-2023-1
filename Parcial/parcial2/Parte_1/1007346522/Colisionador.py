import numpy as np
import math
import matplotlib.pyplot as plt

class Colisionador():

    def __init__(self,RadioColsionador,RadioParticula,NumeroParticulas=1e4):

        self.N = int(NumeroParticulas)

        if RadioColsionador> RadioParticula:
            self.R = RadioColsionador
            self.r = RadioParticula
        else:
            print('Las particulas son más grandes que el colisionador, lol')
            print('Se toman valores por defecto, R = 10, r = 0.1')

            self.R = 10
            self.r = 0.1

        self.X = np.random.uniform(-self.R,self.R,2*self.N)
        self.Y = np.random.uniform(-self.R,self.R,2*self.N)

        self.Prob = np.pi/4*(self.R-self.r)/(self.R)**2

    
    def Validez(self):
        Validos = (self.X**2+self.Y**2)**(0.5) <= self.R-self.r
        return Validos

    
    def Probability_Inside(self):
        Validez = self.Validez()
        P = Validez.sum()/(self.N)
        return P
    
    def Probability_colision(self):
        Validez = self.Validez()
        Val = Validez[:self.N] & Validez[self.N:]
        X = self.X[:self.N]
        Y = self.Y[:self.N]
        X0 = self.X[self.N:]
        Y0 = self.Y[self.N:]

        Col = np.vectorize(lambda x,y,x0,y0: True if math.dist([x,y],[x0,y0])<2*self.r else False)\
                            (X[Val],Y[Val],X0[Val],Y0[Val])
        return Col.sum()/Val.sum()
        
    def Graf(Ns,Prob):

        fig = plt.figure(figsize=(6,6))

        plt.scatter(Ns,Prob,alpha= 0.5,linewidths=0.1)

        plt.savefig('Plot_MonteCarlo_Probab_Colision.png')
